package com.example.demo.services;

import com.example.demo.models.Product;
import com.example.demo.repositories.ProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ProductService {

    private ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> getTop3(int cardNum) {
        return productRepository.getTop3(cardNum);
    }

    public Double getAmount(String date) {
        List<Product> productList = productRepository.getAmount(date);
        double amount = 0.0;
        for (Product product : productList) {
            amount += product.getCount() * product.getPrice();
        }
        return amount;
    }
}
