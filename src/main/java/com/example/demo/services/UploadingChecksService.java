package com.example.demo.services;

import com.example.demo.models.Product;
import com.example.demo.models.Sale;
import com.example.demo.models.Sales;
import com.example.demo.repositories.ProductRepository;
import com.example.demo.repositories.SaleRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.sql.Timestamp;

@Service
public class UploadingChecksService {

    private final ProductRepository productRepository;
    private final SaleRepository saleRepository;

    @Value("${app.path}")
    private String path;

    public UploadingChecksService(ProductRepository productRepository,
                                  SaleRepository saleRepository) {
        this.productRepository = productRepository;
        this.saleRepository = saleRepository;
    }

    @Scheduled(fixedRate = 1000 * 60 * 10)
    void uploadingChecks() {
        File files = new File(path);
        for (File f : files.listFiles()) {
            int i = f.getName().lastIndexOf('.');
            String ext = "";
            if (i > 0) {
                ext = f.getName().substring(i + 1);
            }
            if (f.isFile() && ext.equals("xml")) {
                try {
                    Unmarshaller unmarshaller = JAXBContext.newInstance(Sales.class).createUnmarshaller();
                    File xml = new File(f.getAbsolutePath());
                    Sales sales = (Sales) unmarshaller.unmarshal(xml);

                    for (Sale sale : sales.getSaleList()) {
                        sale.setDate(new Timestamp(Long.parseLong(sale.getUNIXdate())));
                        saleRepository.save(sale);
                        for (Product product : sale.getProducts().getProductList()) {
                            product.setSaleId(sale.getId());
                            productRepository.save(product);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // после обработки файл можно удалить/переместить в другую папку/сделать запись в бд что уже обработан
                f.delete();
            }
        }
    }
}

