package com.example.demo.repositories;

import com.example.demo.models.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepository extends CrudRepository<Product, Long> {

    @Query(value = "SELECT m.id, m.code, m.name, m.price, m.count, m.sale_id FROM (\n" +
                        "SELECT p.id, p.code, p.name, p.price, p.count, p.sale_id,\n" +
                        "       row_number() over(PARTITION BY s.card_number ORDER BY count(1) desc) AS rn\n" +
                        "  FROM maxi.sale s, \n" +
                        "           maxi.product p \n" +
                        " WHERE p.sale_id = s.id\n" +
                        "   AND s.card_number = :card_num\n" +
                        "GROUP BY s.card_number, \n" +
                        "         p.code,\n" +
                        "         p.name) m\n" +
                     "WHERE rn < 4", nativeQuery = true)
    List<Product> getTop3(@Param("card_num") int numCard);

    @Query(value = "SELECT p.id, p.code, p.name, p.price, p.count, p.sale_id\n" +
            "FROM maxi.sale s, maxi.product p\n" +
            "WHERE p.sale_id = s.id\n" +
            "AND s.date BETWEEN STR_TO_DATE(:date, '%Y-%m-%d') AND STR_TO_DATE(:date, '%Y-%m-%d') + 1 - interval '1' second",
            nativeQuery = true)
    List<Product> getAmount(@Param("date") String date);
}
