package com.example.demo.controllers;

import com.example.demo.models.Product;
import com.example.demo.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class IndexController {

    private ProductService productService;

    @Autowired
    public IndexController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/")
    public ModelAndView index() {
        return new ModelAndView("index");
    }

    @GetMapping("/top3")
    public ModelAndView getTop3(@RequestParam String cardNum) {
        int num = 0;
        try {
            num = Integer.parseInt(cardNum);
        } catch (Exception e) {
            //Log
        }
        List<Product> productListTop3 = productService.getTop3(num);
        ModelAndView modelAndView = new ModelAndView("top3");
        modelAndView.addObject("listTop3", productListTop3);
        return modelAndView;
    }

    @GetMapping("/getAmountForDay")
    public ModelAndView getAmountForDay(@RequestParam String date) {
        Double amount = productService.getAmount(date);
        ModelAndView modelAndView = new ModelAndView("amount");
        modelAndView.addObject("date", date);
        modelAndView.addObject("amount", amount);
        return modelAndView;
    }
}
