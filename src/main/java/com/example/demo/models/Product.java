package com.example.demo.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@Entity
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Product {

    @GeneratedValue
    @Id
    private int id;

    private int saleId;

    @XmlElement(name = "PRODUCT_CODE")
    private int code;

    @XmlElement(name = "NAME")
    private String name;

    private double price;

    @XmlElement(name = "COUNT")
    private double count;

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public double getCount() {
        return count;
    }

    public void setSaleId(int saleId) {
        this.saleId = saleId;
    }

    public double getPrice() {
        return price;
    }

    @XmlElement(name = "PRICE")
    public void setPrice(String price) {
        this.price = Double.parseDouble(price.replace(",","."));
    }
}
