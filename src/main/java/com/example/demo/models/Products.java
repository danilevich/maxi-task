package com.example.demo.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "PRODUCTS")
@XmlAccessorType(XmlAccessType.FIELD)
public class Products {

    @XmlElement(name = "PRODUCT")
    private List<Product> productList;

    public List<Product> getProductList() {
        return productList;
    }
}
