package com.example.demo.models;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@XmlAccessorType(XmlAccessType.FIELD)
public class Sale {

    @Id
    @GeneratedValue
    private int id;

    @XmlElement(name = "CARD_NUMBER")
    private String cardNumber;

    @XmlElement(name = "DATE")
    @Transient
    private String UNIXdate;

    private Timestamp date;

    @XmlElement(name = "PRODUCTS")
    @Transient
    private Products products;

    public int getId() {
        return id;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public Products getProducts() {
        return products;
    }

    public String getUNIXdate() {
        return UNIXdate;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }
}
