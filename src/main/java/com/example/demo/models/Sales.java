package com.example.demo.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "SALES")
@XmlAccessorType(XmlAccessType.FIELD)
public class Sales {

    @XmlElement(name = "SALE")
    private List<Sale> saleList;

    public List<Sale> getSaleList() {
        return saleList;
    }
}
